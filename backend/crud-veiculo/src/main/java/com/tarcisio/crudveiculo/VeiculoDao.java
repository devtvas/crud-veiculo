package com.tarcisio.crudveiculo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VeiculoDao extends JpaRepository<Veiculo, Long>{

}
