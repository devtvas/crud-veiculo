import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { VeiculoCreateComponent } from './veiculo-create/veiculo-create.component';
import { VeiculoListComponent } from './veiculo-list/veiculo-list.component';


const routes: Routes = [
  {path:  "", pathMatch:  "full",redirectTo:  "home"},
  {path: "home", component: HomeComponent},
  {path: "veiculo-create", component: VeiculoCreateComponent},
  {path: "veiculo-list", component: VeiculoListComponent}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
