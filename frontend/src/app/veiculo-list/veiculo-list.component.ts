import { Component, OnInit } from '@angular/core';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-veiculo-list',
  templateUrl: './veiculo-list.component.html',
  styleUrls: ['./veiculo-list.component.css']
})
export class VeiculoListComponent implements OnInit {

  veiculos = [];
  veiculoSelecionado;
  constructor(private veiculoService: VeiculoService) { }

  ngOnInit() {
    this.veiculoService.get().subscribe(r => (this.veiculos = r));
  }
  selectVeiculo(v){
    this.veiculoSelecionado = v;
  }

  excluir(id){
    this.veiculoService.delete(id).subscribe(r => (this.veiculoService.get().subscribe(r => (this.veiculos = r))));
  }

}
