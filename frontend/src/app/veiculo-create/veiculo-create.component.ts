import { Component, OnInit } from '@angular/core';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-veiculo-create',
  templateUrl: './veiculo-create.component.html',
  styleUrls: ['./veiculo-create.component.css']
})
export class VeiculoCreateComponent implements OnInit {

  public veiculo = {ano: "", cor: "", marca: "", tipo: "" };
  
  constructor(private veiculoService: VeiculoService) { }

  ngOnInit(): void {
  }
  salvar() {
    this.veiculoService.post(this.veiculo).subscribe(r => (this.veiculo = { ano: "", cor: "", marca: "", tipo: "" }));

  }
}
